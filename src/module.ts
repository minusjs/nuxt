import { addImportsDir, createResolver, defineNuxtModule } from '@nuxt/kit';
import fastGlob from 'fast-glob';
import { basename } from 'pathe';
import { writeFile } from 'node:fs/promises';
import type { NitroEventHandler } from 'nitropack';

export * from './server/ioc';
export * from './server/module';
export * from './server/superstruct';
export type { RequiredKeys } from './types';

export default defineNuxtModule({
  meta: {
    name: 'minus',
  },
  setup(resolvedOptions, nuxt) {
    const { resolve } = createResolver(import.meta.url);
    const typesPath = resolve(nuxt.options.buildDir, 'types/minus-routes.d.ts');

    let modulesRoutes: NitroEventHandler[] = [];
    nuxt.hook('nitro:config', (nitroConfig) => {
      modulesRoutes = (nitroConfig.handlers as NitroEventHandler[]) || [];
    });

    nuxt.hook('prepare:types', async (options) => {
      await generateTypes(nuxt.options.serverDir, typesPath, modulesRoutes);
      options.references.push({
        path: typesPath,
      });
    });

    nuxt.hook('builder:watch', (event, filePath) => {
      if (filePath.startsWith('server/api'))
        return generateTypes(nuxt.options.serverDir, typesPath, modulesRoutes);
    });

    addImportsDir(resolve('./runtime/composables'));
  },
});

async function generateTypes(
  serverDir: string,
  typesPath: string,
  modulesRoutes: NitroEventHandler[],
) {
  const files = await fastGlob(`${serverDir}/api/**/*.ts`);

  // Project routes
  const routes: string[] = [];
  for (const filePath of files) {
    const fullName = filePath.replace(serverDir + '/', '').replace(/\.ts$/, '');

    const name = basename(fullName);
    const method = name.split('.')[1] || 'get';
    const path = fullName.replace(new RegExp(`.${method}$`), '');

    routes.push(
      `'/${path}#${method}': typeof import('~/server/${fullName}').default;`,
    );
  }

  // Modules routes
  for (const nitroHandler of modulesRoutes) {
    routes.push(
      `'${nitroHandler.route}#${
        nitroHandler.method || 'get'
      }': typeof import('${nitroHandler.handler}').default;`,
    );
  }

  const interfaceCode = `
declare module '@minusjs/nuxt' {
  interface MinusRoutes {
    ${routes.join(`\n    `)}
  }
}
export {};
  `;

  await writeFile(typesPath, interfaceCode);
}
