/* eslint-disable @typescript-eslint/no-explicit-any */

export type KeysOfType<T, U> = {
  [K in keyof T]: T[K] extends U ? K : never;
}[keyof T];

export type RequiredKeys<T> = Exclude<
  KeysOfType<T, Exclude<T[keyof T], undefined>>,
  undefined
>;
export type OptionalKeys<T> = Exclude<keyof T, RequiredKeys<T>>;

export type RemoveUndefined<T extends object> = Omit<T, OptionalKeys<T>>;

export type Constructor<T, Arguments extends unknown[] = any[]> = new (
  ...arguments_: Arguments
) => T;

export interface MinusRoutes {}
