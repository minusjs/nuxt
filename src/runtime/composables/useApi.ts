/* eslint-disable @typescript-eslint/no-explicit-any */
import type { NitroFetchOptions } from 'nitropack';
import type { AsyncDataOptions, AsyncData } from 'nuxt/app';
import type { EventHandler } from 'h3';
import type {
  RouteHandler,
  RouteSchemas,
  RequiredKeys,
  MinusRoutes,
} from '@minusjs/nuxt';
import { useAsyncData } from 'nuxt/app';

type ApiRouteOptions = Omit<
  NitroFetchOptions<any, any>,
  'method' | 'body' | 'query' | 'params'
> &
  AsyncDataOptions<any> & { key?: string };

export function useApi<
  RoutePath extends keyof MinusRoutes,
  Response = ApiRouteResponse<MinusRoutes[RoutePath]>,
>(
  route: RoutePath,
  ...args: ApiRouteArgs<MinusRoutes[RoutePath]>
): AsyncData<Response, Error> {
  const [path, method] = route.split('#');

  if (!args[1]) args[1] = {};
  if (!args[1].key) args[1].key = route;

  const handler = () =>
    $fetch<Response>(path, {
      method: method as any,
      ...((args[0] || {}) as any),
      ...args[1],
    });

  return useAsyncData(args[1].key, handler, args[1]) as AsyncData<
    Response,
    Error
  >;
}

type ApiRouteArgs<T extends RouteHandler<any, any, any> | EventHandler<any>> =
  T extends RouteHandler<any, any, any>
    ? RequiredKeys<ApiRoutePayload<T>> extends never
      ? [(ApiRoutePayload<T> | null)?, (ApiRouteOptions | null)?]
      : [ApiRoutePayload<T>, (ApiRouteOptions | null)?]
    : [(any | null)?, (ApiRouteOptions | null)?];

type ApiRoutePayload<T extends RouteHandler<any, any, any>> =
  T extends RouteHandler<any, infer Schemas>
    ? Schemas extends RouteSchemas
      ? {
          [K in keyof Omit<Schemas, 'response'>]: Schemas[K];
        }
      : any
    : any;

type ApiRouteReturn<T extends RouteHandler<any, any, any>> =
  T extends RouteHandler<any, any, infer Return> ? Return : any;

type ApiRouteResponse<
  T extends RouteHandler<any, any, any> | EventHandler<any>,
> = T extends RouteHandler<any, infer Schemas, any>
  ? Schemas extends RouteSchemas
    ? Schemas['response'] extends
        | object
        | string
        | number
        | boolean
        | bigint
        | null
        | undefined
      ? Schemas['response']
      : ApiRouteReturn<T>
    : ApiRouteReturn<T>
  : T extends EventHandler<infer U>
  ? U
  : unknown;
