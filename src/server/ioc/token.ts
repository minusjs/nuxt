import { ProviderFactory } from './factory';
import { Provider } from './provider';

export class InjectionToken<T = unknown> {
  constructor(public readonly name: string) {}

  public use(value: T): Provider<T>;
  public use(factory: ProviderFactory<T>): Provider<T>;
  public use(factoryOrValue: ProviderFactory<T> | T): Provider<T> {
    if (factoryOrValue instanceof ProviderFactory)
      return new Provider(this, factoryOrValue);
    return new Provider<T>(this, new ProviderFactory(() => factoryOrValue));
  }
}

export function token<T>(name: string): InjectionToken<T> {
  return new InjectionToken<T>(name);
}
