import {
  defineFactory,
  FactoryDependencies,
  FactoryInjects,
  ProviderFactory,
} from './factory';
import { Provider } from './provider';
import { InjectionToken, token } from './token';

export interface ContainerOptions {
  imports?: Container[];
  providers?: Provider[];
  exports?: InjectionToken[];
}

export const ContainerToken = token<Container>('Container');

export class Container {
  protected readonly exports: InjectionToken[] = [];
  protected readonly imports: Container[] = [];
  protected readonly contexts: Map<Provider, Container> = new Map();
  protected readonly providers: Map<InjectionToken, Provider> = new Map();
  protected parent: Container;

  constructor({ imports: imports, providers, exports }: ContainerOptions = {}) {
    this.parent = this;
    this.exports = exports || [];

    this.registerValue(ContainerToken, this);

    if (imports) {
      for (const item of imports) {
        this.registerImport(item);
      }
    }

    if (providers) {
      for (const provider of providers) {
        this.registerProvider(provider);
      }
    }
  }

  public registerProvider(provider: Provider, context: Container = this): this {
    this.providers.set(provider.token, provider);
    this.contexts.set(provider, context);
    return this;
  }

  public registerValue(token: InjectionToken, value: unknown): this {
    const factory = defineFactory(() => value);
    const provider = token.use(factory);
    this.registerProvider(provider);
    return this;
  }

  public resolveFactory<T, I extends FactoryInjects>(
    factory: ProviderFactory<T>,
  ): T {
    const injects: Record<string, unknown> = {};
    for (const key in factory.injects) {
      const dependency = factory.injects[key];
      const value = this.resolveProvider(dependency as InjectionToken);
      injects[key] = value;
    }
    return factory.resolve(injects as FactoryDependencies<I>) as T;
  }

  public get<T>(token: InjectionToken<T>): T {
    return this.resolveProvider(token);
  }

  protected registerImport(container: Container): void {
    this.imports.push(container);
    container.parent = this;
    for (const token of container.exports) {
      const provider = container.providers.get(token);
      if (provider) this.registerProvider(provider, container);
    }
  }

  protected resolveProvider<T>(
    token: InjectionToken<T>,
    parents: InjectionToken[] = [],
  ): T {
    parents.push(token);

    const provider = this.providers.get(token);
    if (!provider) throw new Error(`Unable to resolve token ${token.name}`);

    const context = this.contexts.get(provider);
    if (context && context !== this) {
      return context.get(token);
    }

    if (provider.isResolved) return provider.value as T;

    const injects: Record<string, unknown> = {};
    for (const key in provider.factory.injects) {
      const dependency = provider.factory.injects[key];

      if (parents.includes(dependency)) {
        throw new Error(
          `Ciruclar dependencies detected [${this.dependenciesPath([
            ...parents,
            dependency,
          ])}]`,
        );
      }

      const value = this.resolveProvider(dependency, [...parents]);
      injects[key] = value;
    }

    return provider.resolve(injects) as T;
  }

  private dependenciesPath(items: InjectionToken[]): string {
    return items.map((item) => item.name).join(' > ');
  }
}
