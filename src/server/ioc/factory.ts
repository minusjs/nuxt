import { Constructor } from '../../types';
import { InjectionToken } from './token';

export class ProviderFactory<T> {
  constructor(
    public readonly resolve: FactoryFunction<FactoryInjects, T>,
    public readonly injects: FactoryInjects = {},
  ) {}
}

export type FactoryFunction<
  // eslint-disable-next-line @typescript-eslint/ban-types
  Injects extends FactoryInjects = {},
  Return = unknown,
> = (injects: FactoryDependencies<Injects>) => Return;
export type FactoryInjects = Record<string, InjectionToken>;
export type FactoryDependencies<Injects extends FactoryInjects> = {
  [K in keyof Injects]: Injects[K] extends InjectionToken<infer U>
    ? U
    : Injects[K];
};

export function defineFactory<Func extends FactoryFunction>(
  func: Func,
): ProviderFactory<ReturnType<Func>>;
export function defineFactory<
  Injects extends FactoryInjects,
  Func extends FactoryFunction<Injects>,
>(injects: Injects, func: Func): ProviderFactory<ReturnType<Func>>;

export function defineFactory<
  Injects extends FactoryInjects | undefined,
  Func extends Injects extends FactoryInjects
    ? FactoryFunction<Injects>
    : FactoryFunction,
>(
  injectsOrFunc: Injects | FactoryFunction,
  func?: Func,
): ProviderFactory<ReturnType<Func>> {
  return (
    func
      ? new ProviderFactory(func, injectsOrFunc as FactoryInjects)
      : new ProviderFactory(injectsOrFunc as FactoryFunction, {})
  ) as ProviderFactory<ReturnType<Func>>;
}

export type ConstructorInjects<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  T extends Constructor<any>,
  P = ConstructorParameters<T>,
> = {
  [K in keyof P]: InjectionToken<P[K]>;
};

export function defineFactoryClass<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  C extends Constructor<any>,
  A extends ConstructorInjects<C> = ConstructorInjects<C>,
>(
  Ctor: C,
  args: A,
): ProviderFactory<C extends Constructor<infer T> ? T : never> {
  const injects: Record<string, InjectionToken> = {};
  for (const index in args) {
    injects[index.toString()] = args[index];
  }
  return new ProviderFactory(
    (deps) => new Ctor(...Object.values(deps)),
    injects,
  );
}
