/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  EventHandler,
  H3Event,
  getHeaders,
  getMethod,
  defineEventHandler,
  readBody,
  readMultipartFormData,
  getQuery,
} from 'h3';
import { Container, ContainerOptions } from './ioc/container';
import {
  FactoryDependencies,
  FactoryFunction,
  FactoryInjects,
  ProviderFactory,
} from './ioc/factory';

export class Module extends Container {
  constructor(private readonly options: ModuleOptions = {}) {
    super(options);
  }

  public route<Return>(
    handler: RouteHandler<any, any, Return>,
  ): RouteHandler<{}, {}, Return>;
  public route<
    Injects extends FactoryInjects,
    Schemas extends RouteSchemas,
    Handler extends RouteHandler<Injects, Schemas, any> = RouteHandler<
      Injects,
      Schemas
    >,
  >(
    options: RouteOptions<Injects, Schemas>,
    handler: Handler,
  ): RouteHandler<Injects, Schemas, ReturnType<Handler>>;

  public route<
    Injects extends FactoryInjects,
    Schemas extends RouteSchemas,
    Return = any,
  >(
    optionsOrHandler:
      | RouteOptions<Injects, RouteSchemas>
      | RouteHandler<Injects, Schemas, Return>,
    handler?: RouteHandler<Injects, Schemas, Return>,
  ): RouteHandler<Injects, Schemas, Return> {
    const options = handler
      ? (optionsOrHandler as RouteOptions<Injects, RouteSchemas>)
      : {};
    const userHandler =
      handler || (optionsOrHandler as RouteHandler<Injects, Schemas>);

    const factory = new RouteFactory(
      options,
      (injects) => {
        return async (event: H3Event) => {
          const routeEvent = await buildRouteEvent(event);

          if (options.schemas?.pre) {
            const { body, query, params } = await options.schemas.pre(
              routeEvent,
            );
            routeEvent.body = body;
            routeEvent.query = query;
            routeEvent.params = params;
          }

          const output = await userHandler({
            ...(injects as FactoryDependencies<Injects>),
            event: routeEvent,
          });

          if (options.schemas?.post)
            return options.schemas.post(routeEvent, output);
          return output;
        };
      },
      options.injects,
    );

    return defineEventHandler(async (event) => {
      if (this.options.modules) {
        const modules = await Promise.all(this.options.modules());
        for (const mod of modules) {
          this.registerImport(mod.default);
        }
      }

      const handler = this.resolveFactory(factory);
      return handler(event);
    }) as unknown as RouteHandler<Injects, Schemas, Return>;
  }
}

export interface ModuleOptions extends ContainerOptions {
  modules?: () => Promise<{ default: Module }>[];
}

export function defineModule(options: ModuleOptions = {}): Module {
  return new Module(options);
}

async function buildRouteEvent(event: H3Event): Promise<RouteEvent<any>> {
  const headers = getHeaders(event);

  let body: unknown = {};
  const methodsWithBody = ['POST', 'PUT', 'PATCH'];
  if (methodsWithBody.includes(getMethod(event))) {
    body = headers['content-type']?.includes('multipart/form-data')
      ? await readMultipartFormData(event)
      : await readBody(event);
  }

  return {
    body: body || {},
    query: getQuery(event),
    params: event.context.params,
    headers,
    h3: event,
  };
}

export interface RouteHandler<
  Injects extends FactoryInjects,
  Schemas extends RouteSchemas,
  Return = any,
> {
  (context: RouteContext<Injects, Schemas>): Return;
}

type RouteContext<
  Injects extends FactoryInjects,
  Schemas extends RouteSchemas,
> = FactoryDependencies<Injects> & { event: RouteEvent<Schemas> };

export interface RouteEvent<Schemas extends RouteSchemas> {
  h3: H3Event;
  body: Schemas['body'];
  query: Schemas['query'];
  params: Schemas['params'];
  headers: Record<string, string | undefined>;
  // cookies: Record<string, string>; // TODO
}

export interface RouteSchemas {
  query?: any;
  body?: any;
  params?: any;
  response?: unknown;
}

export interface SchemasHandler<Schemas extends RouteSchemas> {
  pre(event: RouteEvent<Schemas>): Schemas;
  post(
    event: RouteEvent<Schemas>,
    output: Schemas['response'],
  ): Schemas['response'];
}

interface RouteOptions<
  Injects extends FactoryInjects,
  Schemas extends RouteSchemas,
> {
  injects?: Injects;
  schemas?: SchemasHandler<Schemas>;
}

export class RouteFactory<
  T extends EventHandler = EventHandler,
> extends ProviderFactory<T> {
  constructor(
    public readonly options: RouteOptions<FactoryInjects, RouteSchemas>,
    public readonly resolve: FactoryFunction<FactoryInjects, T>,
    public readonly injects: FactoryInjects = {},
  ) {
    super(resolve, injects);
  }
}
