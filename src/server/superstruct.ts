/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Struct, Infer, assert } from 'superstruct';
import { OptionalKeys } from '../types';
import { RouteEvent, SchemasHandler } from './module';

interface StructSchemas {
  body?: Struct<any, any>;
  query?: Struct<any, any>;
  params?: Struct<any, any>;
  response?: Struct<any, any>;
}

type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export type InferSchemas<Schemas extends StructSchemas> = {
  [K in keyof Schemas]: Schemas[K] extends Struct<any, any>
    ? Infer<Schemas[K]>
    : any;
};

function assetCoerce(value: any, schema: Struct<any, any>): any {
  const data = schema.create(value);
  assert(data, schema);
  return data;
}

function cleanResponse(output: any, schema?: Struct<any, any>) {
  if (schema) {
    const masked = schema.mask(output);
    return schema.create(masked);
  }
  return output;
}

function validateRequestPayload<Schemas extends StructSchemas>(
  event: RouteEvent<Schemas>,
  { query, params, body }: Schemas,
) {
  const payload = {
    body: event.body,
    params: event.params,
    query: event.query,
  } as InferSchemas<Schemas>;

  if (query) payload.query = assetCoerce(event.query, query);
  if (params) payload.params = assetCoerce(event.params, params);
  if (body) payload.body = assetCoerce(event.body, body);

  return payload;
}

export function superstruct<Schemas extends StructSchemas>(
  schemas: Schemas,
): SchemasHandler<
  Optional<InferSchemas<Schemas>, OptionalKeys<InferSchemas<Schemas>>>
> {
  return {
    pre: (event) => validateRequestPayload(event, schemas),
    post: (_, output) => cleanResponse(output, schemas.response),
  };
}
