import { superstruct } from '../../../src/module';
import { object, optional, string } from 'superstruct';
import { dev } from '../app/dev.module';

export default dev.route(
  {
    schemas: superstruct({
      query: optional(
        object({
          input: optional(string()),
        }),
      ),
      response: object({
        foo: string(),
        bar: string(),
      }),
    }),
  },
  () => {
    return {
      foo: 'bar',
    };
  },
);
