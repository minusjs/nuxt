import { dev } from '../app/dev.module';

export default dev.route(() => {
  return {
    foo: 'bar',
  };
});
