module.exports = {
  extends: [
    'eslint:recommended',
    '@nuxt/eslint-config',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended',
  ],
};
